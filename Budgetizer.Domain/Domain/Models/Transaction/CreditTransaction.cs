﻿namespace Budgetizer.Domain.Models.Transaction
{
    using TransferModels;

    public class CreditTransaction : Transaction
    {
        private CreditTransaction()
            : base()
        {

        }

        public CreditTransaction(TransactionInputDto newTransaction)
            : base(newTransaction)
        {

        }

        public override decimal ApplyTransaction(decimal currentAmount)
        {
            return currentAmount + Amount;
        }
    }
}
