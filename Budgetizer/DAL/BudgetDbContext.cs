﻿namespace Budgetizer.DAL
{
    using Domain.Models.Account;
    using Domain.Models.Category;
    using Domain.Models.Transaction;
    using Microsoft.EntityFrameworkCore;

    public class BudgetDbContext : DbContext
    {
        public BudgetDbContext(DbContextOptions options)
            : base(options)
        {
        }

        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
