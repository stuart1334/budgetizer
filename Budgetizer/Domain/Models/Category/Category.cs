﻿namespace Budgetizer.Domain.Models.Category
{
    using System;

    public class Category
    {
        private Category()
        {

        }

        public Category(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
        }

        public Guid Id { get; private set; }
        public string Name { get; private set; }
    }
}
