﻿namespace Budgetizer.Domain.Models.Transaction
{
    using TransferModels;

    public class DebitTransaction : Transaction
    {
        private DebitTransaction()
            : base()
        {

        }

        public DebitTransaction(TransactionInputDto newTransaction)
            : base(newTransaction)
        {

        }

        public override decimal ApplyTransaction(decimal currentAmount)
        {
            return currentAmount - Amount;
        }
    }
}
