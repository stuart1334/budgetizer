﻿namespace Budgetizer.Domain.Models.Transaction
{
    using System;
    using System.Diagnostics.CodeAnalysis;
    using Account;
    using Budgetizer.TransferModels.Transaction;
    using Category;

    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
    public abstract class Transaction
    {
        protected Transaction()
        {

        }

        protected Transaction(TransactionInputDto newTransaction)
        {
            Id = Guid.NewGuid();
            Amount = newTransaction.Amount;
            Note = newTransaction.Note;
            AccountId = newTransaction.AccountId;
            CategoryId = newTransaction.CategoryId;
        }

        public Guid Id { get; private set; }
        public decimal Amount { get; private set; }
        public string Note { get; private set; }

        private DateTime _date;
        public DateTime Date => _date.Date;

        public Guid AccountId { get; private set; }
        public Account Account { get; private set; }

        public Guid CategoryId { get; private set; }
        public Category Category { get; private set; }

        public abstract decimal ApplyTransaction(decimal currentAmount);
    }
}
