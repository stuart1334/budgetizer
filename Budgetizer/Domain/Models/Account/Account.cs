﻿namespace Budgetizer.Domain.Models.Account
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using Enums;

    [SuppressMessage("ReSharper", "UnusedAutoPropertyAccessor.Local")]
    public class Account
    {
        private Account()
        {

        }

        public Account(string name)
        {
            Id = Guid.NewGuid();
            Name = name;
        }

        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public AccountStatus Status { get; private set; }

        private List<Transaction.Transaction> _transactions = new List<Transaction.Transaction>();

        public IReadOnlyList<Transaction.Transaction> Transactions => _transactions;

        public decimal GetAccountBalance()
        {
            if (Status == AccountStatus.Closed)
            {
                return 0;
            }

            decimal balance = 0;

            foreach (var transaction in _transactions)
            {
                balance = transaction.ApplyTransaction(balance);
            }

            return balance;
        }

        public void AddTransaction(Transaction.Transaction transaction)
        {
            if (Status == AccountStatus.Closed)
            {
                throw new Exception("Can't add transaction to closed account");
            }

            // Check this transaction won't drop balance below 0
            var newBalance = transaction.ApplyTransaction(GetAccountBalance());

            if (newBalance > 0)
            {
                throw new Exception("Account balance cannot drop below 0");
            }

            _transactions.Add(transaction);
        }

        public void CloseAccount()
        {
            if (GetAccountBalance() != 0)
            {
                throw new Exception("Account balance must be 0 before closing");
            }

            Status = AccountStatus.Closed;
        }
    }
}
