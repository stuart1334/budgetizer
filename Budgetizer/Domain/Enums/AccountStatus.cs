﻿namespace Budgetizer.Domain.Enums
{
    public enum AccountStatus
    {
        Open,
        Closed
    }
}
