﻿namespace Budgetizer.Controllers
{
    using System;
    using System.Threading.Tasks;
    using Budgetizer.Service;
    using Budgetizer.TransferModels.Transaction;
    using Microsoft.AspNetCore.Mvc;

    public class AccountController : Controller
    {
        private readonly IAccountService _accountService;

        public AccountController(IAccountService accountService)
        {
            _accountService = accountService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAccount(Guid accountId)
        {
            var account = await _accountService.GetAccountAsync(accountId);

            if (account == null)
            {
                return NotFound();
            }

            return Ok(account);
        }

        [HttpPost]
        public async Task<IActionResult> AddTransactionToAccount(Guid accountId, [FromBody] TransactionInputDto transaction)
        {
            try
            {
                if (await _accountService.AddCreditTransactionToAccount(accountId, transaction))
                {
                    return Ok();
                }
                else
                {
                    return BadRequest();
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}
