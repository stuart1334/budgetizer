﻿using AutoMapper;
using Budgetizer.Domain.Models.Account;
using Budgetizer.TransferModels.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Budgetizer.ModelMapping
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<Account, AccountOutputDto>();
        }
    }
}
