﻿using AutoMapper;
using Budgetizer.Domain.Models.Transaction;
using Budgetizer.TransferModels.Transaction;

namespace Budgetizer.ModelMapping
{
    public class TransactionProfile : Profile
    {
        public TransactionProfile()
        {
            CreateMap<Transaction, TransactionOutputDto>()
                .ForMember(dest => dest.Category, options => options.MapFrom(src => src.Category.Name));
        }
    }
}
