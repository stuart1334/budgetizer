﻿namespace Budgetizer.TransferModels.Transaction
{
    using System;

    public class TransactionInputDto
    {
        public decimal Amount { get; set; }
        public string Note { get; set; }
        public Guid AccountId { get; set; }
        public Guid CategoryId { get; set; }
    }
}
