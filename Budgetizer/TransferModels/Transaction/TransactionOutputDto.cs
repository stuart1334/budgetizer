﻿namespace Budgetizer.TransferModels.Transaction
{
    public class TransactionOutputDto
    {
        public decimal Amount { get; set; }
        public string Note { get; set; }
        public string Category { get; set; }
    }
}
