﻿using Budgetizer.Domain.Enums;
using Budgetizer.TransferModels.Transaction;
using System.Collections.Generic;

namespace Budgetizer.TransferModels.Account
{
    public class AccountOutputDto
    {
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public AccountStatus Status { get; set; }
        IEnumerable<TransactionOutputDto> Transactions { get; set; }
    }
}
