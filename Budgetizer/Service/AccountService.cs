﻿using AutoMapper;
using Budgetizer.DAL;
using Budgetizer.Domain.Models.Transaction;
using Budgetizer.TransferModels.Account;
using Budgetizer.TransferModels.Transaction;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Budgetizer.Service
{
    public class AccountService : IAccountService
    {
        private readonly BudgetDbContext _dbContext;
        private readonly IMapper _mapper;

        public AccountService(BudgetDbContext dbContext,
                              IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<bool> AddCreditTransactionToAccount(Guid accountId, TransactionInputDto newTransaction)
        {
            var account = await _dbContext.Accounts.FirstOrDefaultAsync(a => a.Id == accountId);

            if (account == null)
            {
                throw new Exception("Account not found");
            }

            // Load related transactions
            await _dbContext.Transactions.Where(t => t.AccountId == accountId).LoadAsync();

            var transaction = new CreditTransaction(newTransaction);

            try
            {
                account.AddTransaction(transaction);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public async Task<AccountOutputDto> GetAccountAsync(Guid accountId)
        {
            var account = await _dbContext.Accounts.FirstOrDefaultAsync(a => a.Id == accountId);

            // Load related transactions
            await _dbContext.Transactions.Where(t => t.AccountId == accountId).LoadAsync();

            var accountDto = _mapper.Map<AccountOutputDto>(account);
            accountDto.Amount = account.GetAccountBalance();

            return accountDto;
        }
    }
}
