﻿using Budgetizer.TransferModels.Account;
using Budgetizer.TransferModels.Transaction;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Budgetizer.Service
{
    public interface IAccountService
    {
        public Task<AccountOutputDto> GetAccountAsync(Guid accountId);
        public Task<bool> AddCreditTransactionToAccount(Guid accountId, TransactionInputDto newTransaction);
    }
}
